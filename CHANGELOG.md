# Application Context changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2021-11-11 13:39.

## Version [1.0.8](https://gitlab.com/ultreiaio/application-context/-/milestones/9)

**Closed at 2021-11-11.**


### Issues
  * [[enhancement 9]](https://gitlab.com/ultreiaio/application-context/-/issues/9) **Remove deprecated java code and move to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 10]](https://gitlab.com/ultreiaio/application-context/-/issues/10) **Rethink API and stop using reflection, generate the same compile type safe code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.7](https://gitlab.com/ultreiaio/application-context/-/milestones/8)

**Closed at 2020-03-02.**


### Issues
  * [[enhancement 8]](https://gitlab.com/ultreiaio/application-context/-/issues/8) **Downgrade to java8, but make it usable for later jdk** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.6](https://gitlab.com/ultreiaio/application-context/-/milestones/7)

**Closed at 2020-02-17.**


### Issues
  * [[enhancement 7]](https://gitlab.com/ultreiaio/application-context/-/issues/7) **Migrates to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.5](https://gitlab.com/ultreiaio/application-context/-/milestones/6)

**Closed at 2020-01-25.**


### Issues
  * [[enhancement 6]](https://gitlab.com/ultreiaio/application-context/-/issues/6) **Be able to exclude some components at init** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.4](https://gitlab.com/ultreiaio/application-context/-/milestones/5)

**Closed at 2019-12-11.**


### Issues
  * [[bug 5]](https://gitlab.com/ultreiaio/application-context/-/issues/5) **Fix log in supplier!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/ultreiaio/application-context/-/milestones/4)

**Closed at 2019-12-11.**


### Issues
  * [[bug 4]](https://gitlab.com/ultreiaio/application-context/-/issues/4) **Fix memory leak when closing context and improve code design** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/application-context/-/milestones/3)

**Closed at 2019-11-05.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/application-context/-/issues/3) **Add requireNotNull annotation parameter** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/application-context/-/milestones/2)

**Closed at 2019-10-28.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/application-context/-/issues/1) **Improve how to instantiate components** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 2]](https://gitlab.com/ultreiaio/application-context/-/issues/2) **Use real type in hints and dependencies annotation values** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/ultreiaio/application-context/-/milestones/1)

**Closed at 2019-10-22.**


### Issues
No issue.

