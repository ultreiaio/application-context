package fr.ird.observe.client;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@GenerateApplicationComponent(name = "My first application component")
public class MyService {

    private static final Logger log = LogManager.getLogger(MyService.class);

    public void helloWorld() {
        int i=0;
        String message = String.format("Hello world %1$d !", i);
        System.out.println(message);
        log.info(message);
    }
}
