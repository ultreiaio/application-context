package fr.ird.observe.client;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;

@GenerateApplicationComponent(
        name = "My second application component",
        dependencies = MyService.class,
        hints = IService.class

)
public class MySecondService implements IService {

    private final MyService myService;

    public MySecondService(MyService myService) {
        this.myService = myService;
    }

    public void helloWorld() {
        myService.helloWorld();
    }
}
