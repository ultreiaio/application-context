package fr.ird.observe.client;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.application.context.ApplicationContext;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MySecondServiceTest {

    @BeforeClass
    public static void setUp() {
        MyServiceTest.initApplicationContext();
    }

    @AfterClass
    public static void end() {
        MyServiceTest.closeApplicationContext();
    }

    @Test
    public void test() {
        MySecondService value = MySecondServiceApplicationComponent.value();
        Assert.assertNotNull(value);
        value.helloWorld();
    }

    @Test
    public void test2() {
        IService value = ApplicationContext.get().getComponentValue(IService.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value instanceof MySecondService);
        value.helloWorld();
    }

}
