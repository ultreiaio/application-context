package io.ultreia.java4all.application.context;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SingletonSupplier;

/**
 * Application supplier for a given application component.
 * <p>
 * This supplier is used in generated application component to cache a unique instance.
 * <p>
 * A such supplier will be cleared when the application context is closed if created by method {@link ApplicationContext#componentSupplier(Class, Class)}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ApplicationContext#componentSupplier(Class, Class)
 * @since 1.0.3
 */
public class ApplicationComponentSupplier<O, C extends ApplicationComponent<O>> extends SingletonSupplier<C> {

    /**
     * Component value type.
     */
    private final Class<O> componentValueType;

    protected ApplicationComponentSupplier(Class<O> componentValueType, Class<C> componentType) {
        super(() -> ApplicationContext.get().getComponentTyped(componentValueType, componentType), true);
        this.componentValueType = componentValueType;
    }

    @Override
    public String toString() {
        return super.toString() + " → " + componentValueType.getName();
    }

}
