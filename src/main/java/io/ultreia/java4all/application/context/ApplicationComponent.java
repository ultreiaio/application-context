package io.ultreia.java4all.application.context;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.application.context.spi.GenerateApplicationComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;


/**
 * Application component is a unique instance shared by {@link ApplicationContext}.
 * <p>
 * To generate a such component, just place the {@link GenerateApplicationComponent} annotation on it.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see GenerateApplicationComponent
 * @since 1.0.0
 */
public class ApplicationComponent<O> implements Supplier<O>, Closeable {

    private static final Logger log = LogManager.getLogger(ApplicationComponent.class);

    /**
     * Name of the component.
     */
    private final String name;

    /**
     * Delegate singleton supplier to get component value.
     */
    private final ApplicationComponentValueSupplier<O> supplier;

    protected ApplicationComponent(String name, ApplicationComponentValueSupplier<O> supplier) {
        this.name = Objects.requireNonNull(name);
        this.supplier = Objects.requireNonNull(supplier);
    }

    @Override
    public O get() {
        return supplier.get();
    }

    public String getName() {
        return name;
    }

    public Class<O> getComponentType() {
        return supplier.componentType();
    }

    public void setValue(O value) {
        supplier.setSupplier(() -> value);
    }

    @Override
    public void close() throws IOException {
        log.info("Closing component: " + this);
        if (supplier.withValue()) {
            if (Closeable.class.isAssignableFrom(getComponentType())) {
                ((Closeable) supplier.get()).close();
            }
            supplier.clear();
        }
    }

    public boolean withDependencies() {
        return !getDependencies().isEmpty();
    }

    public List<Class<?>> getDependencies() {
        return supplier.dependencies();
    }

    public List<Class<?>> getHints() {
        return supplier.hints();
    }

    public String toString() {
        return String.format("%s → %s", name, getComponentType().getName());
    }

}
