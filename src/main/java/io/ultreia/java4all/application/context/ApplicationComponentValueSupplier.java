package io.ultreia.java4all.application.context;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.3
 */
public class ApplicationComponentValueSupplier<O> extends SingletonSupplier<O> implements Closeable {

    private static final Logger log = LogManager.getLogger(ApplicationComponentValueSupplier.class);

    private final Class<O> componentType;
    /**
     * All dependencies (registered also as application components).
     */
    private final List<Class<?>> dependencies;
    /**
     * All hints to get this component via {@link ApplicationContext}.
     */
    private final List<Class<?>> hints;

    public static class Builder<O> {

        private final Class<O> componentType;
        /**
         * All dependencies (registered also as application components).
         */
        private final List<Class<?>> dependencies;
        /**
         * All hints to get this component via {@link ApplicationContext}.
         */
        private final List<Class<?>> hints;
        private boolean requireNotNull;
        private Supplier<O> supplier;

        public Builder(Class<O> componentType) {
            this.componentType = Objects.requireNonNull(componentType);
            dependencies = new LinkedList<>();
            hints = new LinkedList<>();
        }

        public Builder<O> requireNotNull() {
            this.requireNotNull = true;
            return this;
        }

        public Builder<O> setSupplier(Supplier<O> supplier) {
            this.supplier = Objects.requireNonNull(supplier);
            return this;
        }

        public Builder<O> setDependencies(Class<?>... dependencies) {
            Collections.addAll(this.dependencies, dependencies);
            return this;
        }

        public Builder<O> setHints(Class<?>... hints) {
            Collections.addAll(this.hints, hints);
            return this;
        }

        public ApplicationComponentValueSupplier<O> build() {
            return new ApplicationComponentValueSupplier<>(componentType, dependencies, hints, requireNotNull, supplier);
        }
    }

    public static <O> Builder<O> on(Class<O> componentType) {
        return new Builder<>(componentType);
    }

    public ApplicationComponentValueSupplier(Class<O> componentType, List<Class<?>> dependencies, List<Class<?>> hints, boolean requireNotNull, Supplier<O> supplier) {
        super(supplier, requireNotNull);
        this.componentType = Objects.requireNonNull(componentType);
        this.dependencies = Collections.unmodifiableList(Objects.requireNonNull(dependencies));
        this.hints = Collections.unmodifiableList(Objects.requireNonNull(hints));
    }

    public List<Class<?>> dependencies() {
        return dependencies;
    }

    public List<Class<?>> hints() {
        return hints;
    }

    @Override
    public O get() {
        if (!withValue()) {
            log.info(String.format("Create component value from supplier: %s - %s", componentType.getName(), this));
        }
        return super.get();
    }

    @Override
    public Optional<O> clear() {
        if (withValue()) {
            log.info(String.format("Clear component value: %s", componentType.getName()));
        }
        return super.clear();
    }

    @Override
    public void close() {
        clear();
    }

    public Class<O> componentType() {
        return componentType;
    }
}
