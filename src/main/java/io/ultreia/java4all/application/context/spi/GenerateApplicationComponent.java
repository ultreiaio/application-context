package io.ultreia.java4all.application.context.spi;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)

public @interface GenerateApplicationComponent {

    /**
     * @return name of component
     */
    String name();

    /**
     * @return strategy used to instantiate component value
     */
    ApplicationComponentInstantiateStrategy instantiateStrategy() default ApplicationComponentInstantiateStrategy.CONSTRUCTOR;

    /**
     * @return all hints usable to get this component from {@link io.ultreia.java4all.application.context.ApplicationContext}
     */
    Class[] hints() default {};

    /**
     * @return dependencies of this component (used only with {@link ApplicationComponentInstantiateStrategy#CONSTRUCTOR})
     */
    Class[] dependencies() default {};

    /**
     * @return class of value supplier (used only with {@link ApplicationComponentInstantiateStrategy#SUPPLIER})
     */
    Class instantiateSupplier() default Void.class;

    /**
     * @return {@code true} if an interface {@code WithXXX} should be generate to have a nice way to access your component
     */
    boolean generateInterface() default true;

    boolean requireNotNull() default true;

}
