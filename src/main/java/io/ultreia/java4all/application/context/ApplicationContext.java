package io.ultreia.java4all.application.context;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import io.ultreia.java4all.util.ServiceLoaders;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class ApplicationContext implements Closeable {

    private static final Logger log = LogManager.getLogger(ApplicationContext.class);

    /** Unique instance of client application context */
    private static ApplicationContext INSTANCE;
    /** Registered component suppliers (this must be static since instances in applicationComponent is static...) */
    private static final List<ApplicationComponentSupplier<?, ? extends ApplicationComponent<?>>> COMPONENT_SUPPLIERS = new LinkedList<>();
    /** Registered components */
    private final List<ApplicationComponent<?>> components;
    /** Registered components indexed by their hints */
    private final Map<Class<?>, ApplicationComponent<?>> componentsCache;
    /** Computed rounds in reverse precedence order (used to close components) */
    private final List<MutableInt> componentsRoundInReverseOrder;
    /** Registered components indexed by their precedence round */
    private final ListMultimap<MutableInt, ApplicationComponent<?>> componentsByRound;
    /** Lock object on this context. */
    private final Object lock;
    /** Flag to know when context is closing */
    protected boolean closed;

    public static boolean isInit() {
        return INSTANCE != null;
    }

    public static ApplicationContext get() {
        return Objects.requireNonNull(INSTANCE, "No client application context found.");
    }

    public static <O, C extends ApplicationComponent<O>> ApplicationComponentSupplier<O, C> componentSupplier(Class<O> componentValueType, Class<C> componentType) {
        ApplicationComponentSupplier<O, C> result = new ApplicationComponentSupplier<>(componentValueType, componentType);
        COMPONENT_SUPPLIERS.add(result);
        return result;
    }

    public ApplicationContext() {
        this(Collections.emptySet());
    }

    public ApplicationContext(Class<?> excludeComponent) {
        this(Collections.singleton(Objects.requireNonNull(excludeComponent)));
    }

    public ApplicationContext(Set<Class<?>> excludeComponents) {
        INSTANCE = this;
        log.info(">>");
        log.info(">> Init application context.");
        log.info(">>");
        this.lock = new Object();
        this.components = new LinkedList<>();
        this.componentsCache = new LinkedHashMap<>();

        Map<Class<?>, ApplicationComponent<?>> componentMap = new LinkedHashMap<>();
        for (ApplicationComponent<?> component : ServiceLoaders.reload(ApplicationComponent.class)) {
            if (excludeComponents != null && excludeComponents.contains(component.getComponentType())) {
                log.info(String.format("Exclude application component: %s", component));
                continue;
            }
            componentMap.put(component.getComponentType(), component);
        }
        registerComponents(componentMap);
        this.componentsByRound = computeComponentsReverseOrder();
        List<MutableInt> componentsRoundInReverseOrder = new LinkedList<>(componentsByRound.keySet());
        Collections.reverse(componentsRoundInReverseOrder);
        this.componentsRoundInReverseOrder = Collections.unmodifiableList(componentsRoundInReverseOrder);
        int index = 0;
        int round = 0;
        int componentsCount = components.size();
        for (Map.Entry<MutableInt, Collection<ApplicationComponent<?>>> entry : componentsByRound.asMap().entrySet()) {
            int roundIndex = 0;
            int roundCount = entry.getValue().size();
            for (ApplicationComponent<?> component : entry.getValue()) {
                log.info(String.format("Component (round %3d - %3d/%3d) - %3d/%3d: %s", round, ++roundIndex, roundCount, ++index, componentsCount, component));
            }
            round++;

        }
        log.info("<<");
        log.info("<< Init application context.");
        log.info("<<");
    }

    //-------------------------------------------
    // - Locks
    //-------------------------------------------

    public void lock() throws InterruptedException {
        synchronized (lock) {
            lock.wait();
        }
    }

    public void releaseLock() {
        synchronized (lock) {
            lock.notifyAll();
        }
    }

    //-------------------------------------------
    // - Closeable
    //-------------------------------------------

    @Override
    public void close() {
        log.info(">>");
        log.info(">> Closing application context.");
        log.info(">>");
        closed = true;
        List<ApplicationComponent<?>> componentsToUnregister = new LinkedList<>();
        if (componentsByRound != null) {
            int index = 0;
            int count = components.size();
            for (MutableInt round : componentsRoundInReverseOrder) {
                for (ApplicationComponent<?> component : componentsByRound.get(round)) {
                    try {
                        log.info(String.format("Closing component: (%3d/%3d) %s", ++index, count, component));
                        component.close();
                        componentsToUnregister.add(component);
                    } catch (IOException e) {
                        log.error("Can't close component: " + component, e);
                    }
                }
            }
            index = 0;
            for (ApplicationComponent<?> component : componentsToUnregister) {
                log.info(String.format("Unregister component: (%3d/%3d) %s", ++index, count, component.getName()));
                unregisterComponent(component);
            }
        }
        int index = 0;
        int count = COMPONENT_SUPPLIERS.size();
        for (ApplicationComponentSupplier<?, ?> componentSupplier : COMPONENT_SUPPLIERS) {
            log.info(String.format("Remove component supplier: (%3d/%3d) %s", ++index, count, componentSupplier));
            componentSupplier.clear();
        }

        INSTANCE = null;
        log.info("<<");
        log.info("<< Closing application context.");
        log.info("<<");
    }

    /**
     * @return {@code true} si le context applicatif a été fermé (et est donc
     * passé dans la méthode {@link #close()}, {@code false} autrement.
     */
    public boolean isClosed() {
        return closed;
    }

    //-------------------------------------------
    // - Components
    //-------------------------------------------

    public <O, C extends ApplicationComponent<O>> C getComponentTyped(Class<O> componentValueType, Class<C> componentType) {
        return componentType.cast(getComponent(componentValueType));
    }

    public <O> ApplicationComponent<O> getComponent(Class<O> componentType) {
        ApplicationComponent<O> result = getComponentFromCache(Objects.requireNonNull(componentType));
        if (result == null) {
            result = getComponentFromComponents(componentType);
            if (result != null) {
                componentsCache.put(componentType, result);
            }
        }
        return Objects.requireNonNull(result, "Can't find component of type: " + componentType.getName());
    }

    public <O> O getComponentValue(Class<O> componentType) {
        ApplicationComponent<O> result = getComponent(Objects.requireNonNull(componentType));
        return Objects.requireNonNull(result, "Can't find component of type: " + componentType.getName()).get();
    }

    protected void registerComponents(Map<Class<?>, ApplicationComponent<?>> componentMap) {
        for (Map.Entry<Class<?>, ApplicationComponent<?>> entry : componentMap.entrySet()) {
            ApplicationComponent<?> component = entry.getValue();
            log.debug(String.format("Register component (%3d): %s", components.size(), component));
            registerComponent(component);
        }
    }

    protected <O> void registerComponent(ApplicationComponent<O> component) {
        Class<O> componentType = Objects.requireNonNull(component).getComponentType();
        ApplicationComponent<O> existingComponent = getComponentFromComponents(componentType);
        if (existingComponent != null) {
            throw new IllegalArgumentException(String.format("There is already a such component for type: %s", componentType.getName()));
        }
        int index = components.size();
        log.info(String.format("Register component (%3d): %s", index, component));
        components.add(component);
        componentsCache.put(componentType, component);
        for (Class<?> extraBinding : component.getHints()) {
            if (componentsCache.containsKey(extraBinding)) {
                throw new IllegalArgumentException(String.format("There is already a such component %s with hint: %s", componentsCache.get(extraBinding), extraBinding.getName()));
            }
            log.info(String.format("Register component (%3d): %s {hint → '%s'}", index, component, extraBinding.getName()));
            componentsCache.put(extraBinding, component);
        }
    }

    private <O> void unregisterComponent(ApplicationComponent<O> component) {
        log.debug(String.format("Unregister component: %s", component));
        components.remove(component);
        componentsCache.entrySet().removeIf(entry -> component.equals(entry.getValue()));
    }

    @SuppressWarnings("unchecked")
    private <O> ApplicationComponent<O> getComponentFromCache(Class<O> componentType) {
        return (ApplicationComponent<O>) componentsCache.get(Objects.requireNonNull(componentType));
    }

    private <O> ApplicationComponent<O> getComponentFromComponents(Class<O> componentType) {
        for (ApplicationComponent<?> component : components) {
            if (componentType.isAssignableFrom(component.getComponentType())) {
                //noinspection unchecked
                return (ApplicationComponent<O>) component;
            }
        }
        return null;
    }

    private ListMultimap<MutableInt, ApplicationComponent<?>> computeComponentsReverseOrder() {

        ListMultimap<MutableInt, ApplicationComponent<?>> result = ArrayListMultimap.create();

        int round = 0;

        // first pass with all components with no dependencies

        List<ApplicationComponent<?>> componentsSafe = components.stream().filter(e -> !e.withDependencies()).collect(Collectors.toCollection(LinkedList::new));

        // Map of all available components indexed by their hints
        Set<Class<?>> componentsAvailable = new LinkedHashSet<>();
        fillAvailableComponents(componentsAvailable, componentsSafe);

        // Components to process
        List<ApplicationComponent<?>> componentsToProcess = new LinkedList<>(components);
        log.info(String.format("Scan for round: %3d - found %d components out of %d", round, componentsSafe.size(), componentsToProcess.size()));
        {
            int roundIndex = 0;
            int roundCount = componentsSafe.size();
            for (ApplicationComponent<?> component : componentsSafe) {
                log.info(String.format("Scan for round: %3d (%3d/%3d) - found component %s", round, ++roundIndex, roundCount, component));
            }
        }
        componentsToProcess.removeAll(componentsSafe);
        int componentsCount = components.size();
        log.info(String.format("Scan for round: %3d - remaining %d components out of %d", round, componentsToProcess.size(), componentsCount));
        result.putAll(new MutableInt(round), componentsSafe);

        do {
            round++;
            int componentsToProcessCount = componentsToProcess.size();
            componentsSafe = processComponents(componentsToProcess, componentsAvailable);
            if (componentsSafe.isEmpty()) {
                if (!componentsToProcess.isEmpty()) {
                    throw new IllegalStateException(String.format("Could not find any more components on round %d, with remaining components: %s", round, componentsToProcess));
                }
                break;
            }
            log.info(String.format("Scan for round: %3d - found %3d components out of %d", round, componentsSafe.size(), componentsToProcessCount));
            result.putAll(new MutableInt(round), componentsSafe);
            fillAvailableComponents(componentsAvailable, componentsSafe);
            int roundIndex = 0;
            int roundCount = componentsSafe.size();
            for (ApplicationComponent<?> component : componentsSafe) {
                log.info(String.format("Scan for round: %3d  (%3d/%3d) - found component %s", round, ++roundIndex, roundCount, component));
            }
            if (!componentsToProcess.isEmpty()) {
                log.info(String.format("Scan for round: %3d - remaining %3d components out of %d", round, componentsToProcess.size(), componentsCount));
            }
        }
        while (!componentsToProcess.isEmpty());
        return Multimaps.unmodifiableListMultimap(result);
    }

    private List<ApplicationComponent<?>> processComponents(List<ApplicationComponent<?>> componentsToProcess, Set<Class<?>> componentsAvailable) {
        List<ApplicationComponent<?>> result = new LinkedList<>();
        Iterator<ApplicationComponent<?>> iterator = componentsToProcess.iterator();
        while (iterator.hasNext()) {
            ApplicationComponent<?> component = iterator.next();
            List<Class<?>> dependencies = component.getDependencies();
            if (componentsAvailable.containsAll(dependencies)) {
                result.add(component);
                iterator.remove();
            }
        }
        return result;
    }

    private void fillAvailableComponents(Set<Class<?>> componentsAvailable, List<ApplicationComponent<?>> result) {
        for (ApplicationComponent<?> component : result) {
            Set<Class<?>> collect = componentsCache.entrySet().stream().filter(e -> Objects.equals(component, e.getValue())).map(Map.Entry::getKey).collect(Collectors.toSet());
            componentsAvailable.addAll(collect);
            componentsAvailable.add(component.getComponentType());
        }
    }

}
