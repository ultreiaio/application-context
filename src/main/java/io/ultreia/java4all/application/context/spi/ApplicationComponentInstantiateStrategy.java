package io.ultreia.java4all.application.context.spi;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

public enum ApplicationComponentInstantiateStrategy {
    /**
     * Use constructor with optional {@link GenerateApplicationComponent#dependencies()} as constructor parameters.
     * <p>
     * This is the default behaviour.
     *
     * @see GenerateApplicationComponent#dependencies()
     */
    CONSTRUCTOR,
    /**
     * Use unique service loader instance.
     *
     * @see io.ultreia.java4all.util.ServiceLoaders#loadUniqueService(Class)
     */
    SERVICE_LOADER,
    /**
     * Use a supplier.
     *
     * @see GenerateApplicationComponent#instantiateSupplier()
     */
    SUPPLIER
}
