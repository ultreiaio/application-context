package io.ultreia.java4all.application.context.spi;

/*-
 * #%L
 * Application context
 * %%
 * Copyright (C) 2019 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.application.context.ApplicationComponent;
import io.ultreia.java4all.application.context.ApplicationComponentSupplier;
import io.ultreia.java4all.application.context.ApplicationComponentValueSupplier;
import io.ultreia.java4all.application.context.ApplicationContext;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.util.ServiceLoaders;
import io.ultreia.java4all.util.SingletonSupplier;

import javax.annotation.Generated;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Consumer;

@SupportedAnnotationTypes("io.ultreia.java4all.application.context.spi.GenerateApplicationComponent")
@AutoService(Processor.class)
@SupportedOptions({"debug", "quiet"})
public class GenerateApplicationComponentProcessor extends AbstractProcessor {

    private static final String COMPONENT_JAVA_FILE = "package %1$s;\n" +
            "\n" +
            "%2$s" +
            "\n@AutoService(value = ApplicationComponent.class)" +
            "\n@Generated(value = \"%3$s\", date = \"%4$s\")" +
            "\npublic class %5$sApplicationComponent extends ApplicationComponent<%5$s> {\n" +
            "\n" +
            "    public static ApplicationComponentSupplier<%5$s, %5$sApplicationComponent> INSTANCE = ApplicationContext.componentSupplier(%5$s.class, %5$sApplicationComponent.class);\n\n" +
            "    public static %5$sApplicationComponent component() {\n" +
            "        return INSTANCE.get();\n" +
            "    }\n\n" +
            "    public static %5$s value() {\n" +
            "        return component().get();\n" +
            "    }\n\n" +
            "    public %5$sApplicationComponent() {\n" +
            "        %7$s\n" +
            "    }\n" +
            "}\n";

    private static final String INTERFACE_JAVA_FILE = "package %1$s;\n" +
            "\n" +
            "%2$s" +
            "\n@Generated(value = \"%3$s\", date = \"%4$s\")" +
            "\npublic interface With%5$s {\n" +
            "\n" +
            "    default %5$s get%5$s() {\n" +
            "        return %5$sApplicationComponent.value();\n" +
            "    }\n" +
            "}\n";

    private final Set<String> done = new TreeSet<>();
    private Elements elementUtils;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        elementUtils = processingEnv.getElementUtils();
        for (TypeElement annotation : annotations) {

            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element annotatedElement : annotatedElements) {
                TypeElement classElement = (TypeElement) annotatedElement;

                String fullyQualifiedName = classElement.getQualifiedName().toString();

                String packageName = processingEnv.getElementUtils().getPackageOf(classElement).toString();
                String fullClassName = fullyQualifiedName.substring(packageName.length() + 1);
                String className = fullClassName;
                int i = className.indexOf(".");
                if (i > -1) {
                    className = className.substring(i + 1);
                }
                String generatedClassName = packageName + "." + className + "ApplicationComponent";

                if (!done.add(generatedClassName)) {

                    // Already done
                    logWarning(String.format("Skip already processed class: %s", generatedClassName));
                    continue;
                }

                logDebug(String.format("Detect application component: %s", classElement));

                Map<String, AnnotationValue> elementValues = getAnnotation(classElement);

                try {
                    generateDefinitionFile(packageName, generatedClassName, fullClassName, className, elementValues);
                } catch (IOException e) {
                    throw new RuntimeException("Can't generate JavaBean definition file for: " + classElement, e);
                }
                if (Boolean.parseBoolean(elementValues.get("generateInterface").toString())) {
                    try {
                        String generatedInterfaceSimpleName = "With" + className;
                        String generatedInterfaceName = packageName + "." + generatedInterfaceSimpleName;
                        generateInterfaceFile(packageName, generatedInterfaceName, className);
                    } catch (IOException e) {
                        throw new RuntimeException("Can't generate JavaBean definition file for: " + classElement, e);
                    }
                }
            }
        }
        return true;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    private void generateInterfaceFile(String packageName, String generatedClassName, String className) throws IOException {

        ImportManager importManager = new ImportManager(packageName);
        importManager.addImport(Generated.class);
        importManager.addImport(ApplicationContext.class);
        importManager.addImport(ApplicationComponent.class);
        importManager.addImport(SingletonSupplier.class);

        String imports = importManager.getImportsSection("\n");
        String content = String.format(INTERFACE_JAVA_FILE,
                                       packageName,
                                       imports,
                                       getClass().getName(),
                                       new Date(),
                                       className);
        generate(generatedClassName, content);
    }

    private void generateDefinitionFile(String packageName, String generatedClassName, String fullClassName, String className, Map<String, AnnotationValue> elementValues) throws IOException {

        ImportManager importManager = new ImportManager(packageName);
        importManager.addImport(Generated.class);
        importManager.addImport(AutoService.class);
        importManager.addImport(ApplicationContext.class);
        importManager.addImport(ApplicationComponent.class);
        importManager.addImport(ApplicationComponentSupplier.class);
        if (!className.equals(fullClassName)) {
            importManager.addImport(packageName + "." + fullClassName);
        }
        String name = elementValues.get("name").toString();
        importManager.addImport(ApplicationComponentValueSupplier.class);
        List<String> builderOperations = new ArrayList<>();
        builderOperations.add(String.format("on(%s.class)", className));
        boolean requireNotNull = Boolean.parseBoolean(elementValues.get("requireNotNull").toString());
        if (requireNotNull) {
            builderOperations.add("requireNotNull()");
        }
        ApplicationComponentInstantiateStrategy instantiateStrategy = ApplicationComponentInstantiateStrategy.valueOf(elementValues.get("instantiateStrategy").getValue().toString());
        List<?> hints = (List<?>) elementValues.get("hints").getValue();
        if (hints.size() > 0) {
            loadClasses(builderOperations, importManager, hints, "setHints(%s)", null);
        }
        boolean useDependencies = instantiateStrategy == ApplicationComponentInstantiateStrategy.CONSTRUCTOR;

        List<String> dependenciesBuilder = new ArrayList<>();
        List<?> dependencies = (List<?>) elementValues.get("dependencies").getValue();
        if (dependencies.size() > 0) {
            loadClasses(builderOperations, importManager, dependencies, "setDependencies(%s)", clazz -> {
                if (useDependencies) {
                    dependenciesBuilder.add(String.format("ApplicationContext.get().getComponentValue(%s)", clazz));
                }
            });
        }
        switch (instantiateStrategy) {
            case CONSTRUCTOR:
                if (dependenciesBuilder.isEmpty()) {
                    builderOperations.add(String.format("setSupplier(%s::new)", className));
                } else {
                    builderOperations.add(String.format("setSupplier(() -> new %s(%s))", className, String.join(", ", dependenciesBuilder)));
                }
                break;
            case SERVICE_LOADER:
                importManager.addImport(ServiceLoaders.class);
                builderOperations.add(String.format("setSupplier(() -> ServiceLoaders.loadUniqueService(%s.class))", className));
                break;
            case SUPPLIER:
                Object instantiateSupplier = elementValues.get("instantiateSupplier").getValue();
                TypeElement typeMirror = elementUtils.getTypeElement(instantiateSupplier.toString());
                importManager.addImport(typeMirror.getQualifiedName().toString());

                builderOperations.add(String.format("setSupplier(new %s())", typeMirror.getSimpleName()));
                break;
            default:
                throw new IllegalStateException("Can't use it now!!!");
        }
        builderOperations.add("build()");

        String constructor = String.format("super(%1$s, ApplicationComponentValueSupplier\n                .%2$s);", name, String.join("\n                .", builderOperations));
        String imports = importManager.getImportsSection("\n");
        String content = String.format(COMPONENT_JAVA_FILE,
                                       packageName,
                                       imports,
                                       getClass().getName(),
                                       new Date(),
                                       className,
                                       name,
                                       constructor);
        generate(generatedClassName, content);
    }

    private void loadClasses(List<String> builderContent, ImportManager importManager, List<?> dependencies, String message, Consumer<String> optionalConsumer) {
        List<String> classes = new ArrayList<>();
        for (Object dependency : dependencies) {
            TypeElement typeMirror = elementUtils.getTypeElement(dependency.toString().substring(0, dependency.toString().length() - 6));
            importManager.addImport(typeMirror.getQualifiedName().toString());
            String clazz = typeMirror.getSimpleName() + ".class";
            classes.add(clazz);
            if (optionalConsumer != null) {
                optionalConsumer.accept(clazz);
            }
        }
        builderContent.add(String.format(message, String.join(", ", classes)));
    }


    private void generate(String generatedClassName, String content) throws IOException {
        logInfo(String.format("Generate: %s", generatedClassName));
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(generatedClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            out.print(content);
        }
    }

    private void logDebug(String msg) {
        if (processingEnv.getOptions().containsKey("debug")) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
        }
    }

    private void logWarning(String msg) {
//        if (processingEnv.getOptions().containsKey("debug")) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, msg);
//        }
    }

    private void logInfo(String msg) {
        if (!processingEnv.getOptions().containsKey("quiet")) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
        }
    }

    private Map<String, AnnotationValue> getAnnotation(TypeElement classElement) {
        List<? extends AnnotationMirror> list = elementUtils.getAllAnnotationMirrors(classElement);
        TypeMirror typeMirror = elementUtils.getTypeElement(GenerateApplicationComponent.class.getName()).asType();
        AnnotationMirror annotationMirror = list.stream().filter(a -> a.getAnnotationType().equals(typeMirror)).findFirst().orElseThrow(() -> new IllegalStateException(String.format("Can't find annotation %s on class %s", GenerateApplicationComponent.class.getName(), classElement.getQualifiedName())));
        Map<? extends ExecutableElement, ? extends AnnotationValue> elementValuesWithDefaults = elementUtils.getElementValuesWithDefaults(annotationMirror);
        Map<String, AnnotationValue> result = new TreeMap<>();
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : elementValuesWithDefaults.entrySet()) {
            AnnotationValue value = entry.getValue();
            result.put(entry.getKey().getSimpleName().toString(), value);
        }
        return result;
    }
}

