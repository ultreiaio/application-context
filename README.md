# Application context

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/application-context.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22application-context%22)
[![Build Status](https://gitlab.com/ultreiaio/application-context/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/application-context/pipelines)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

This project offers a simple API to share application components via a single shared instance of an application context.

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/application-context/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/application-context)

# Community

* [Contact](mailto:incoming+ultreiaio/application-context@gitlab.com)
